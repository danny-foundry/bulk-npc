import {dialogForm} from "./helpers/dialogs.js";

function getDocumentPath(document) {
    let path = [];

    while (document !== null) {
        path.push(document.data.name);
        document = document.folder;
    }

    return path.reverse().join(" / ");
}

const Api = new (function() {
    this.clone = async function(actorId, quantity) {
        const actor = Actors
            .instance
            .get(actorId);

        for (let i = 0; i < quantity; i ++) {
            const duplicate = await actor.clone({}, {
                save: true,
            });

            const name = actor.data.name + ' ' + (i + 1);
            duplicate.update({
                name: name,
                data: {
                    cloned_from: actor.data._id,
                },
                token: {
                    name: name,
                }
            });
        }
    };

    this.bulkCopy = async function() {
        let options = await dialogForm('Bulk Copy', '/modules/bulk-npc/templates/dialogs/bulk-copy-actor.html', {
            actors: Actors
                .instance
                .entries
                .filter((actor) => !actor.data.data.cloned_from)
                .map((actor) => {
                    actor.path = getDocumentPath(actor);
                    return actor;
                }),
        });

        await this.clone(options.source_actor, parseInt(options.quantity));
    };

    this.bulkDelete = async function() {
        let options = await dialogForm('Bulk Delete', '/modules/bulk-npc/templates/dialogs/bulk-delete-actors.html', {
            actors: Actors
                .instance
                .entries
                .map((actor) => {
                    actor.path = getDocumentPath(actor);
                    return actor;
                }),
        });

        let deletes = [];
        options.actors.forEach((actor) => {
            deletes.push(Actors.instance.get(actor).delete());
        });
        await Promise.all(deletes);
    };
})();

Hooks.once('init', () => {
    window.bulkNpc = Api;
});

/* Example dialog usage */
/*
const dialog = new Dialog({
    title: 'Bulk Copy',
    content: '<p>Select an actor to clone and the number of instances to create. <span class="dynamic">NOT LOADED.</span></p>',
    buttons: {
        okay: {
            icon: '<i class="fas fa-check"></i>',
            label: 'Okay',
            callback() {
                console.log('Okay!');
            }
        }
    },
    render($html) {
        $html.find('.dynamic').text('LOADED!');
    },
}, {
    jQuery: true,
});
dialog.render(true);
 */