import {formToObject} from "./objects.js";

export function dialogForm(title, template, data = {})
{
    return new Promise((resolve, reject) => {
        $.get(template, async (content) => {
            let compiled = Handlebars.compile(content);
            await Dialog.prompt({
                title: title,
                content: compiled(data),
                callback: async (dialog) => {
                    resolve(formToObject($('form', dialog)));
                }
            });
        });
    });
}